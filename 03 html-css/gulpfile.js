'use strict';

const gulp = require('gulp');
const { src, dest } = require('gulp');
const { watch } = require('gulp');

const pug = require('gulp-pug');
const sass = require('gulp-sass');
const image = require('gulp-image');

function buildHtml(cb) {
  return gulp.src('src/*.pug')
        .pipe(pug({
          pretty: true
          }))
        .pipe(dest('build/'));
  cb();
}

function buildCss(cb) {
  return gulp.src('src/css/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(dest('build/css'));
  cb();
}

function buildImg(cb) {
  return gulp.src('src/img/*')
        .pipe(image())
        .pipe(dest('build/img'));
  cb();
}

function defaultTask(cb) {
  console.log('hihihi');
  watch(['src/**/*.pug'], buildHtml);
  watch(['src/**/*.scss'], buildCss);
  watch(['src/img/*'], buildImg);
  cb();
}





exports.default = defaultTask
